package com.example.healthplanner.helpers;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class DatabaseHandler
{
    private final FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
    private final DatabaseReference databaseReference = firebaseDatabase.getReference();

    public static DatabaseHandler instance;

    public static DatabaseHandler getInstance()
    {
        if(instance == null)
        {
            instance = new DatabaseHandler();
        }

        return instance;
    }

    public DatabaseReference getDatabaseReference()
    {
        return databaseReference;
    }
}
