package com.example.healthplanner.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.example.healthplanner.BuildConfig;
import com.example.healthplanner.R;
import com.example.healthplanner.helpers.UtilsValidators;
import com.example.healthplanner.interfaces.ActivitiesFragmentsCommunication;
import com.example.healthplanner.models.User;
import com.google.firebase.auth.FirebaseAuth;

public class LogInFragment extends Fragment
{
    public static final String TAG_LOGIN = "TAG_LOGIN";

    private ActivitiesFragmentsCommunication fragmentCommunication;
    private FirebaseAuth auth;
    private EditText edtTextEmail;
    private EditText edtTextPassword;

    public static LogInFragment newInstance()
    {
        Bundle args = new Bundle();

        LogInFragment fragment = new LogInFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        auth = FirebaseAuth.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_log_in, container, false);

        edtTextEmail = view.findViewById(R.id.edt_email_login);
        edtTextPassword = view.findViewById(R.id.edt_password_login);

        SharedPreferences sharedPreferences = requireContext().getSharedPreferences("mainPrefs", Context.MODE_PRIVATE);

        String email = sharedPreferences.getString("Email", "");
        String password = sharedPreferences.getString("Password", "");

        edtTextEmail.setText(email);
        edtTextPassword.setText(password);

        if (auth.getCurrentUser() != null)
        {
            User.getInstance().initializeUserFromDatabase();
            fragmentCommunication.openHomeFragment();
        }

        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    void save(String key, EditText text)
    {
        SharedPreferences sharedPreferences = requireContext().getSharedPreferences("mainPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor myEdit = sharedPreferences.edit();
        myEdit.putString(key, text.getText().toString());

        myEdit.apply();
    }

    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    @Override
    public void onPause()
    {
        super.onPause();

        save("Email", edtTextEmail);
        save("Password", edtTextPassword);
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);

        if (context instanceof ActivitiesFragmentsCommunication)
        {
            fragmentCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.btn_login).setOnClickListener(v ->
        {
            if(!validateEmailAndPassword())
            {
                return;
            }

            EditText edtEmail = view.findViewById(R.id.edt_email_login);
            String email = edtEmail.getText().toString();

            EditText edtPassword = view.findViewById(R.id.edt_password_login);
            String password = edtPassword.getText().toString();

            auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(authTask ->
            {
                if (authTask.isSuccessful())
                {
                    User.getInstance().initializeUserFromDatabase();
                    fragmentCommunication.openHomeActivity();
                    Toast.makeText(getContext(), "Successfully logged in!", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(getContext(), "Logging failed! Authentication error.", Toast.LENGTH_SHORT).show();
                }
            });
        });

        view.findViewById(R.id.button_forgotPassword).setOnClickListener(v -> fragmentCommunication.openForgotPasswordFragment());

        view.findViewById(R.id.btn_back_login).setOnClickListener(v -> fragmentCommunication.openWelcomeFragment());
    }

    private boolean validateEmailAndPassword()
    {
        if (getView() == null)
        {
            return false;
        }

        EditText emailEdtTxt = getView().findViewById(R.id.edt_email_login);
        EditText passwordEdtTxt = getView().findViewById(R.id.edt_password_login);

        String email = emailEdtTxt.getText().toString();
        String password = passwordEdtTxt.getText().toString();

        if (!UtilsValidators.isValidEmail(email))
        {
            emailEdtTxt.setError("Invalid email");
            return false;
        }
        else
        {
            emailEdtTxt.setError(null);
        }

        if (!UtilsValidators.isValidPassword(password))
        {
            passwordEdtTxt.setError("Invalid password");
            return false;
        }
        else
        {
            passwordEdtTxt.setError(null);
        }
        return true;
    }
}
