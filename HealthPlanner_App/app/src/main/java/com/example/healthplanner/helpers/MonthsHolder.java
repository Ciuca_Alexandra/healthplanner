package com.example.healthplanner.helpers;

import com.example.healthplanner.models.Month;

import java.util.ArrayList;

public class MonthsHolder
{
    private static final ArrayList<Month> initialMonths = new ArrayList<>();

    static
    {
        initialMonths.add(new Month("Ianuarie", 1, new ArrayList<>()));
        initialMonths.add(new Month("Februarie", 2, new ArrayList<>()));
        initialMonths.add(new Month("Martie", 3, new ArrayList<>()));
        initialMonths.add(new Month("Aprilie", 4, new ArrayList<>()));
        initialMonths.add(new Month("Mai", 5, new ArrayList<>()));
        initialMonths.add(new Month("Iunie", 6, new ArrayList<>()));
        initialMonths.add(new Month("Iulie", 7, new ArrayList<>()));
        initialMonths.add(new Month("August", 8, new ArrayList<>()));
        initialMonths.add(new Month("Septembrie", 9, new ArrayList<>()));
        initialMonths.add(new Month("Octombrie", 10, new ArrayList<>()));
        initialMonths.add(new Month("Noiembrie", 11, new ArrayList<>()));
        initialMonths.add(new Month("Decembrie", 12, new ArrayList<>()));
    }

    public static ArrayList<Month> getInitialMonths()
    {
        return initialMonths;
    }
}
