package com.example.healthplanner.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.healthplanner.R;
import com.example.healthplanner.fragments.ForgotPasswordFragment;
import com.example.healthplanner.fragments.LogInFragment;
import com.example.healthplanner.fragments.ProfileFragment;
import com.example.healthplanner.fragments.SignUpFragment;
import com.example.healthplanner.fragments.WelcomeFragment;
import com.example.healthplanner.interfaces.ActivitiesFragmentsCommunication;
import com.example.healthplanner.models.Appointment;
import com.example.healthplanner.models.Month;

public class MainActivity extends AppCompatActivity implements ActivitiesFragmentsCommunication
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            openWelcomeFragment();
        }
    }

    @Override
    public void openWelcomeFragment()
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        FragmentTransaction replaceTransaction = transaction.replace(R.id.frame_layout, WelcomeFragment.newInstance(), "TAG_WELCOME");
        replaceTransaction.commit();
    }

    @Override
    public void openSignUpFragment()
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        FragmentTransaction replaceTransaction = transaction.replace(R.id.frame_layout, SignUpFragment.newInstance(), "TAG_SIGNUP");
        replaceTransaction.addToBackStack("TAG_SIGNUP");
        replaceTransaction.commit();
    }

    @Override
    public void openProfileFragment()
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        FragmentTransaction replaceTransaction = transaction.replace(R.id.frame_layout, ProfileFragment.newInstance(), "TAG_PROFILE");
        replaceTransaction.addToBackStack("TAG_PROFILE");
        replaceTransaction.commit();
    }

    @Override
    public void openLogInFragment()
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        FragmentTransaction replaceTransaction = transaction.replace(R.id.frame_layout, LogInFragment.newInstance(), "TAG_LOGIN");
        replaceTransaction.addToBackStack("TAG_LOGIN");
        replaceTransaction.commit();
    }

    @Override
    public void openForgotPasswordFragment()
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        FragmentTransaction replaceTransaction = transaction.replace(R.id.frame_layout, ForgotPasswordFragment.newInstance(), "TAG_FORGOT_PASSWORD");
        replaceTransaction.addToBackStack("TAG_FORGOT_PASSWORD");
        replaceTransaction.commit();
    }

    @Override
    public void openHomeFragment()
    {

    }

    @Override
    public void openAccountFragment()
    {

    }

    @Override
    public void openRecommTestsFragment()
    {

    }

    @Override
    public void openNextYearApptFragment()
    {

    }

    @Override
    public void openApptFragment(Month month)
    {

    }

    @Override
    public void openAddApptFragment(Month month)
    {

    }

    @Override
    public void openApptDetailsFragment(Appointment appointment, Month month)
    {

    }

    @Override
    public void openMainActivity()
    {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void openHomeActivity()
    {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }
}