package com.example.healthplanner.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.example.healthplanner.R;
import com.example.healthplanner.helpers.DatabaseHandler;
import com.example.healthplanner.helpers.UtilsValidators;
import com.example.healthplanner.interfaces.ActivitiesFragmentsCommunication;
import com.example.healthplanner.models.Appointment;
import com.example.healthplanner.models.Month;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class AddAppointmentFragment extends Fragment
{
    public static final String TAG_APPOINT_ADD = "TAG_APPOINT_ADD";

    private ActivitiesFragmentsCommunication fragmentsCommunication;
    Month month;

    public static AddAppointmentFragment newInstance(Month month)
    {
        Bundle args = new Bundle();
        args.putParcelable("month", month);

        AddAppointmentFragment fragment = new AddAppointmentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_add_appointment, container, false);

        month = getArguments().getParcelable("month");

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);

        if (context instanceof ActivitiesFragmentsCommunication)
        {
            fragmentsCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.btn_add).setOnClickListener(v ->
        {
            EditText edtTitle = view.findViewById(R.id.edt_apt_add);
            String title = edtTitle.getText().toString();
            if (TextUtils.isEmpty(title))
            {
                edtTitle.setError("Invalid input");
                return;
            }
            else
            {
                edtTitle.setError(null);
            }

            EditText edtHour = view.findViewById(R.id.edt_hour_add);
            String hour = edtHour.getText().toString();
            if (TextUtils.isEmpty(hour))
            {
                edtHour.setError("Invalid input");
                return;
            }
            else if (!UtilsValidators.isValidHour(hour))
            {
                edtHour.setError("Invalid format! Try HH:MM");
                return;
            }
            else
            {
                edtHour.setError(null);
            }

            EditText edtDate = view.findViewById(R.id.edt_date_add);
            String date = edtDate.getText().toString();
            if (TextUtils.isEmpty(date))
            {
                edtDate.setError("Invalid input");
                return;
            }
            else
            {
                edtDate.setError(null);
                if (!ProcessChosenDate(edtDate))
                {
                    return;
                }
            }

            EditText edtDoctor = view.findViewById(R.id.edt_doctor_add);
            String doctor = edtDoctor.getText().toString();
            if (TextUtils.isEmpty(doctor))
            {
                edtDoctor.setText("");
            }
            else
            {
                edtDoctor.setError(null);
            }

            EditText edtPhone = view.findViewById(R.id.edt_phone_add);
            String phone = edtPhone.getText().toString();
            if (TextUtils.isEmpty(phone))
            {
                edtPhone.setText("");
            }
            else if (!UtilsValidators.isValidPhoneNumber(phone))
            {
                edtPhone.setError("Invalid number");
                return;
            }
            else
            {
                edtPhone.setError(null);
            }

            EditText edtAddress = view.findViewById(R.id.edt_address_add);
            String address = edtAddress.getText().toString();
            if (TextUtils.isEmpty(address))
            {
                edtAddress.setText("");
            }
            else
            {
                edtAddress.setError(null);
            }

            if (LocalDate.parse(edtDate.getText().toString()).getYear() > LocalDate.now().getYear())
            {
                DatabaseReference databaseReference = DatabaseHandler.getInstance()
                                                                     .getDatabaseReference()
                                                                     .child("Users")
                                                                     .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                                                     .child("NextYear");
                Appointment newAppointment = new Appointment(title, date, hour, doctor, phone, address);
                databaseReference.child(String.valueOf(newAppointment.getId())).setValue(newAppointment).addOnCompleteListener(task ->
                {
                    if (task.isSuccessful())
                    {
                        Toast.makeText(getContext(), "Incoming appointment added!", Toast.LENGTH_SHORT).show();
                        fragmentsCommunication.openApptFragment(month);
                    }
                    else
                    {
                        Toast.makeText(getContext(), "Incoming appointment adding failed!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            else
            {
                DatabaseReference databaseReference = DatabaseHandler.getInstance().getDatabaseReference()
                                                                     .child("Users")
                                                                     .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                                                     .child("Months")
                                                                     .child(String.valueOf(month.getMonthNr() - 1))
                                                                     .child("appointments");
                Appointment newAppointment = new Appointment(title, date, hour, doctor, phone, address);
                databaseReference.child(String.valueOf(newAppointment.getId())).setValue(newAppointment).addOnCompleteListener(task ->
                {
                    if (task.isSuccessful())
                    {
                        Toast.makeText(getContext(), "Appointment added!", Toast.LENGTH_SHORT).show();
                        fragmentsCommunication.openApptFragment(month);
                    }
                    else
                    {
                        Toast.makeText(getContext(), "Appointment adding failed!", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            edtTitle.setText("");
            edtHour.setText("");
            edtDate.setText("");
            edtDoctor.setText("");
            edtPhone.setText("");
            edtAddress.setText("");
        });

        view.findViewById(R.id.btn_back_add).setOnClickListener(v -> fragmentsCommunication.openApptFragment(month));
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private boolean ProcessChosenDate(EditText edtDate)
    {
        try
        {
            LocalDate date = LocalDate.parse(edtDate.getText().toString());

            if (date.getMonthValue() != month.getMonthNr())
            {
                edtDate.setError("The month is not the one you selected! (" + month.getName() + ")");
                return false;
            }
            if (date.getYear() < LocalDate.now().getYear())
            {
                edtDate.setError("The year is invalid!");
                return false;
            }
        } catch (DateTimeParseException e)
        {
            e.printStackTrace();
            edtDate.setError("Date format invalid! Try yyyy-mm-dd");
            return false;
        }

        return true;
    }
}
