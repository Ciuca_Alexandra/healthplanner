package com.example.healthplanner.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.healthplanner.R;
import com.example.healthplanner.adapters.TestAdapter;
import com.example.healthplanner.helpers.DatabaseHandler;
import com.example.healthplanner.interfaces.ActivitiesFragmentsCommunication;
import com.example.healthplanner.models.Test;
import com.example.healthplanner.models.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.time.LocalDate;
import java.util.ArrayList;

public class RecommendedTestsFragment extends Fragment
{
    public static final String TAG_RECOM_TESTS = "TAG_RECOM_TESTS";

    private ActivitiesFragmentsCommunication fragmentsCommunication;
    private RecyclerView recyclerView;
    private ArrayList<Test> tests = new ArrayList<>();
    TestAdapter testAdapter = new TestAdapter(tests);

    public static RecommendedTestsFragment newInstance()
    {
        Bundle args = new Bundle();

        RecommendedTestsFragment fragment = new RecommendedTestsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_recommended_tests, container, false);

        recyclerView = view.findViewById(R.id.tests_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

        DatabaseHandler.getInstance().getDatabaseReference()
                       .child("Tests")
                       .addValueEventListener(new ValueEventListener()
                       {
                           @RequiresApi(api = Build.VERSION_CODES.O)
                           @Override
                           public void onDataChange(@NonNull DataSnapshot snapshot)
                           {
                               LocalDate dob = LocalDate.parse(User.getInstance().getDateOfBirth());
                               int age = LocalDate.now().getYear() - dob.getYear();
                               if (LocalDate.now().getDayOfYear() < dob.getDayOfYear())
                               {
                                   age--;
                               }

                               tests.clear();
                               for(DataSnapshot ds : snapshot.getChildren())
                               {
                                   Test test = ds.getValue(Test.class);
                                   if (age >= test.getMinAge()
                                           && age <= test.getMaxAge()
                                           && (!test.isForPregnant() || User.getInstance().getPregnant()))
                                   {
                                       tests.add(test);
                                   }
                               }
                               testAdapter.notifyDataSetChanged();
                           }

                           @Override
                           public void onCancelled(@NonNull DatabaseError error)
                           {

                           }
                       });

        recyclerView.setAdapter(testAdapter);

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        if (context instanceof ActivitiesFragmentsCommunication)
        {
            fragmentsCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.btn_back_tests).setOnClickListener(v -> fragmentsCommunication.openHomeFragment());
    }
}
