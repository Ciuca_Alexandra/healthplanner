package com.example.healthplanner.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Appointment implements Parcelable
{
    private static int lastId = 0;
    private int id;
    private String date;
    private String title;
    private String time;
    private String doctorName;
    private String phoneNumber;
    private String address;

    public Appointment()
    {
    }

    public Appointment(String title, String date, String time, String doctorName, String phoneNumber, String address)
    {
        this.id = lastId++;
        this.date = date;
        this.title = title;
        this.time = time;
        this.doctorName = doctorName;
        this.phoneNumber = phoneNumber;
        this.address = address;
    }

    protected Appointment(Parcel parcel)
    {
        id = parcel.readInt();
        date = parcel.readString();
        title = parcel.readString();
        time = parcel.readString();
        doctorName = parcel.readString();
        phoneNumber = parcel.readString();
        address = parcel.readString();
    }

    public static final Creator<Appointment> CREATOR = new Creator<Appointment>()
    {
        @Override
        public Appointment createFromParcel(Parcel in)
        {
            return new Appointment(in);
        }

        @Override
        public Appointment[] newArray(int size)
        {
            return new Appointment[size];
        }
    };

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        if (id > lastId)
        {
            lastId = id + 1;
        }
        this.id = id;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTime()
    {
        return time;
    }

    public void setTime(String time)
    {
        this.time = time;
    }

    public String getDoctorName()
    {
        return doctorName;
    }

    public void setDoctorName(String doctorName)
    {
        this.doctorName = doctorName;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeInt(id);
        parcel.writeString(date);
        parcel.writeString(title);
        parcel.writeString(time);
        parcel.writeString(doctorName);
        parcel.writeString(phoneNumber);
        parcel.writeString(address);
    }
}
