package com.example.healthplanner.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.healthplanner.R;
import com.example.healthplanner.helpers.UtilsValidators;
import com.example.healthplanner.interfaces.ActivitiesFragmentsCommunication;
import com.example.healthplanner.models.User;
import com.google.firebase.auth.FirebaseAuth;

public class SignUpFragment extends Fragment
{
    public static final String TAG_SIGNUP = "TAG_SIGNUP";

    private ActivitiesFragmentsCommunication fragmentCommunication;
    private FirebaseAuth auth;

    public static SignUpFragment newInstance()
    {
        Bundle args = new Bundle();

        SignUpFragment fragment = new SignUpFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        auth = FirebaseAuth.getInstance();
    }

    @Override
    public void onStart()
    {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_sign_up, container, false);
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        if (context instanceof ActivitiesFragmentsCommunication)
        {
            fragmentCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.btn_next).setOnClickListener(v -> {
            if(!validateEmailAndPassword())
            {
                return;
            }

            EditText edtEmail = view.findViewById(R.id.edt_email_signup);
            String email = edtEmail.getText().toString();

            EditText edtPassword = view.findViewById(R.id.edt_password_signup);
            String password = edtPassword.getText().toString();

            EditText firstNameEdtTxt = getView().findViewById(R.id.edt_FirstName_signup);
            String firstName = firstNameEdtTxt.getText().toString();

            EditText lastNameEdtTxt = getView().findViewById(R.id.edt_SecondName_signup);
            String lastName = lastNameEdtTxt.getText().toString();

            auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(authTask ->
            {
                if (authTask.isSuccessful())
                {
                    Toast.makeText(getContext(), "Successfully registered!", Toast.LENGTH_SHORT).show();
                    User.getInstance().setEmail(email);
                    User.getInstance().setFirstName(firstName);
                    User.getInstance().setSecondName(lastName);
                    fragmentCommunication.openProfileFragment();
                } else
                {
                    Toast.makeText(getContext(), "Registration failed! Authentication error.", Toast.LENGTH_SHORT).show();
                }
            });
        });

        view.findViewById(R.id.btn_back_signup).setOnClickListener(v -> fragmentCommunication.openWelcomeFragment());
    }

    private boolean validateEmailAndPassword()
    {
        if (getView() == null)
        {
            return false;
        }

        EditText emailEdtTxt = getView().findViewById(R.id.edt_email_signup);
        EditText passwordEdtTxt = getView().findViewById(R.id.edt_password_signup);
        EditText repeatPasswordEdtTxt = getView().findViewById(R.id.edt_repeat_password);

        String email = emailEdtTxt.getText().toString();
        String password = passwordEdtTxt.getText().toString();
        String repeatPassword = repeatPasswordEdtTxt.getText().toString();

        if (!UtilsValidators.isValidEmail(email))
        {
            emailEdtTxt.setError("Invalid email");
            return false;
        } else
        {
            emailEdtTxt.setError(null);
        }

        if (!UtilsValidators.isValidPassword(password))
        {
            passwordEdtTxt.setError("Invalid password");
            return false;
        } else
        {
            passwordEdtTxt.setError(null);
        }

        if (!UtilsValidators.isSamePassword(password, repeatPassword))
        {
            repeatPasswordEdtTxt.setError("Passwords are not the same");
            return false;
        } else
        {
            repeatPasswordEdtTxt.setError(null);
        }
        return true;
    }
}
