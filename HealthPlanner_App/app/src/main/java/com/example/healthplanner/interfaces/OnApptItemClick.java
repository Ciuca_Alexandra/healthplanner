package com.example.healthplanner.interfaces;

import com.example.healthplanner.models.Appointment;

public interface OnApptItemClick
{
    void onClick(Appointment appointment);
}
