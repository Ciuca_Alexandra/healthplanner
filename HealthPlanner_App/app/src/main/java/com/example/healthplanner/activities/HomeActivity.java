package com.example.healthplanner.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.healthplanner.R;
import com.example.healthplanner.fragments.AccountFragment;
import com.example.healthplanner.fragments.AddAppointmentFragment;
import com.example.healthplanner.fragments.AppointmentDetailsFragment;
import com.example.healthplanner.fragments.AppointmentFragment;
import com.example.healthplanner.fragments.HomeFragment;
import com.example.healthplanner.fragments.NextYearApptFragment;
import com.example.healthplanner.fragments.RecommendedTestsFragment;
import com.example.healthplanner.fragments.WelcomeFragment;
import com.example.healthplanner.interfaces.ActivitiesFragmentsCommunication;
import com.example.healthplanner.models.Appointment;
import com.example.healthplanner.models.Month;
import com.google.firebase.auth.FirebaseAuth;

public class HomeActivity extends AppCompatActivity implements ActivitiesFragmentsCommunication
{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        if (savedInstanceState == null)
        {
            openHomeFragment();
        }
    }

    @Override
    @SuppressLint("RestrictedApi")
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);

        if (menu instanceof MenuBuilder)
        {
            MenuBuilder m = (MenuBuilder) menu;
            m.setOptionalIconsVisible(true);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.account:
                openAccountFragment();
                return true;
            case R.id.recommended_tests:
                openRecommTestsFragment();
                return true;
            case R.id.next_year_appt:
                openNextYearApptFragment();
                return true;
            case R.id.log_out:
                FirebaseAuth.getInstance().signOut();
                Toast.makeText(this, "Logged out", Toast.LENGTH_SHORT).show();
                openMainActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void openWelcomeFragment()
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        FragmentTransaction replaceTransaction = transaction.replace(R.id.frame_layout, WelcomeFragment.newInstance(), "TAG_WELCOME");
        replaceTransaction.commit();
    }

    @Override
    public void openSignUpFragment()
    {

    }

    @Override
    public void openProfileFragment()
    {

    }

    @Override
    public void openLogInFragment()
    {

    }

    @Override
    public void openForgotPasswordFragment()
    {

    }

    @Override
    public void openHomeFragment()
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        FragmentTransaction replaceTransaction = transaction.replace(R.id.home_frame_layout, HomeFragment.newInstance(), "TAG_HOME");
        replaceTransaction.commit();
    }

    @Override
    public void openAccountFragment()
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        FragmentTransaction replaceTransaction = transaction.replace(R.id.home_frame_layout, AccountFragment.newInstance(), "TAG_ACCOUNT");
        replaceTransaction.addToBackStack("TAG_ACCOUNT");
        replaceTransaction.commit();
    }

    @Override
    public void openRecommTestsFragment()
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        FragmentTransaction replaceTransaction = transaction.replace(R.id.home_frame_layout, RecommendedTestsFragment.newInstance(), "TAG_RECOM_TESTS");
        replaceTransaction.addToBackStack("TAG_RECOM_TESTS");
        replaceTransaction.commit();
    }

    @Override
    public void openNextYearApptFragment()
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        FragmentTransaction replaceTransaction = transaction.replace(R.id.home_frame_layout, NextYearApptFragment.newInstance(), "TAG_iNCOM_TESTS");
        replaceTransaction.addToBackStack("TAG_iNCOM_TESTS");
        replaceTransaction.commit();
    }

    @Override
    public void openApptFragment(Month month)
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        FragmentTransaction replaceTransaction = transaction.replace(R.id.home_frame_layout, AppointmentFragment.newInstance(month), "TAG_APPOINT");
        replaceTransaction.addToBackStack("TAG_APPOINT");
        replaceTransaction.commit();
    }

    @Override
    public void openAddApptFragment(Month month)
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        FragmentTransaction replaceTransaction = transaction.replace(R.id.home_frame_layout, AddAppointmentFragment.newInstance(month), "TAG_APPOINT_ADD");
        replaceTransaction.addToBackStack("TAG_APPOINT_ADD");
        replaceTransaction.commit();
    }

    @Override
    public void openApptDetailsFragment(Appointment appointment, Month month)
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        FragmentTransaction replaceTransaction = transaction.replace(R.id.home_frame_layout, AppointmentDetailsFragment.newInstance(appointment, month), "TAG_APPOINT_DET");
        replaceTransaction.addToBackStack("TAG_APPOINT_DET");
        replaceTransaction.commit();
    }

    @Override
    public void openMainActivity()
    {
        Intent myIntent = new Intent(this, MainActivity.class);
        startActivity(myIntent);
        finish();
    }

    @Override
    public void openHomeActivity()
    {
        Intent myIntent = new Intent(this, HomeActivity.class);
        startActivity(myIntent);
        finish();
    }
}