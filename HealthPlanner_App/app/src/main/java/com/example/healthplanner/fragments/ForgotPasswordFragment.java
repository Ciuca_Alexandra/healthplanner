package com.example.healthplanner.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.healthplanner.R;
import com.example.healthplanner.helpers.UtilsValidators;
import com.example.healthplanner.interfaces.ActivitiesFragmentsCommunication;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ForgotPasswordFragment extends Fragment
{
    public static final String TAG_FORGOT_PASSWORD = "TAG_FORGOT_PASSWORD";

    private ActivitiesFragmentsCommunication fragmentCommunication;
    private FirebaseAuth auth;

    public static ForgotPasswordFragment newInstance()
    {
        Bundle args = new Bundle();

        ForgotPasswordFragment fragment = new ForgotPasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        auth = FirebaseAuth.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_forgot_password, container, false);
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);

        if (context instanceof ActivitiesFragmentsCommunication)
        {
            fragmentCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.btn_forgotPassword).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                validateEmail();
            }
        });

        view.findViewById(R.id.btn_back_forgot).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                fragmentCommunication.openLogInFragment();
            }
        });
    }

    private void resetPassword(String email)
    {
        auth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener()
                {
                    @Override
                    public void onComplete(@NonNull Task task)
                    {
                        if(task.isSuccessful())
                        {
                            Toast.makeText(getContext(), "The email was sent successfully!", Toast.LENGTH_SHORT).show();
                            fragmentCommunication.openLogInFragment();
                        } else
                        {
                            Toast.makeText(getContext(), "The email was not sent successfully!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void validateEmail()
    {
        if(getView() == null)
        {
            return;
        }

        EditText emailEdtText = getView().findViewById(R.id.edt_email_forgot);

        String email = emailEdtText.getText().toString().trim();

        if(!UtilsValidators.isValidEmail(email))
        {
            emailEdtText.setError("Invalid Email");
            return;
        } else
        {
            emailEdtText.setError(null);
        }
        resetPassword(email);
    }
}
