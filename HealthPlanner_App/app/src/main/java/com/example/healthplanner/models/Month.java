package com.example.healthplanner.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Month implements Parcelable
{
    private int monthNr;
    private String name;
    private ArrayList<Appointment> appointments;

    public Month()
    {
    }

    public Month(String name, int monthNr, ArrayList<Appointment> appointments)
    {
        this.monthNr = monthNr;
        this.name = name;
        this.appointments = appointments;
    }

    protected Month(Parcel parcel)
    {
        monthNr = parcel.readInt();
        name = parcel.readString();
        appointments = new ArrayList<>();
        parcel.readTypedList(appointments, Appointment.CREATOR);
    }

    public static final Creator<Month> CREATOR = new Creator<Month>()
    {
        @Override
        public Month createFromParcel(Parcel in)
        {
            return new Month(in);
        }

        @Override
        public Month[] newArray(int size)
        {
            return new Month[size];
        }
    };

    public void addAppointment(Appointment appt)
    {
        appointments.add(appt);
    }

    public int getMonthNr()
    {
        return monthNr;
    }

    public void setMonthNr(int monthNr)
    {
        this.monthNr = monthNr;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public ArrayList<Appointment> getAppointments()
    {
        return appointments;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeInt(monthNr);
        parcel.writeString(name);
        parcel.writeArray(appointments.toArray());
    }
}
