package com.example.healthplanner.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.healthplanner.R;
import com.example.healthplanner.models.Test;

import java.util.ArrayList;

public class TestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    ArrayList<Test> tests;

    public TestAdapter(ArrayList<Test> tests)
    {
        this.tests = tests;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.rec_test_item_cell, parent, false);
        TestsViewHolder testsViewHolder = new TestsViewHolder(view);
        return testsViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if (holder instanceof TestsViewHolder)
        {
            Test test = tests.get(position);
            ((TestsViewHolder) holder).bind(test);
        }
    }

    @Override
    public int getItemCount()
    {
        return this.tests.size();
    }


    class TestsViewHolder extends RecyclerView.ViewHolder
    {
        private TextView title;
        private TextView interval;
        private View view;

        TestsViewHolder(View view)
        {
            super(view);
            title = view.findViewById(R.id.test);
            interval = view.findViewById(R.id.interval);
            this.view = view;
        }

        void bind(Test test)
        {
            title.setText(test.getTitle());
            interval.setText(test.getInterval());
        }
    }
}
