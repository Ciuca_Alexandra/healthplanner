package com.example.healthplanner.models;

import androidx.annotation.NonNull;

import com.example.healthplanner.helpers.DatabaseHandler;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.time.LocalDate;

public class User
{
    private static User instance;

    private String firstName;
    private String secondName;
    private String email;
    private String dateOfBirth;
    private Boolean isPregnant;

    public User()
    {
    }

    public static User getInstance()
    {
        if (instance == null)
        {
            instance = new User();
        }

        return instance;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getSecondName()
    {
        return secondName;
    }

    public void setSecondName(String secondName)
    {
        this.secondName = secondName;
    }

    public String getFullName()
    {
        return getFirstName() + " " + getSecondName();
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getDateOfBirth()
    {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth)
    {
        this.dateOfBirth = dateOfBirth;
    }

    public Boolean getPregnant()
    {
        return isPregnant;
    }

    public void setPregnant(Boolean pregnant)
    {
        isPregnant = pregnant;
    }

    public void initializeUserFromDatabase()
    {
        DatabaseHandler.getInstance().getDatabaseReference()
                       .child("Users")
                       .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                       .child("Info")
                       .addValueEventListener(new ValueEventListener()
                       {
                           @Override
                           public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                           {
                               User currentInstance = getInstance();
                               currentInstance.setFirstName(dataSnapshot.child("firstName").getValue(String.class));
                               currentInstance.setSecondName(dataSnapshot.child("secondName").getValue(String.class));
                               currentInstance.setDateOfBirth(dataSnapshot.child("dateOfBirth").getValue(String.class));
                               currentInstance.setEmail(dataSnapshot.child("email").getValue(String.class));
                               currentInstance.setPregnant(dataSnapshot.child("pregnant").getValue(boolean.class));
                           }

                           @Override
                           public void onCancelled(@NonNull DatabaseError databaseError)
                           {
                           }
                       });

        DatabaseHandler.getInstance().getDatabaseReference()
                       .child("Users")
                       .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                       .child("NextYear")
                       .addListenerForSingleValueEvent(new ValueEventListener()
                       {
                           @Override
                           public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                           {
                               for(DataSnapshot ds : dataSnapshot.getChildren())
                               {
                                   Appointment appointment = ds.getValue(Appointment.class);
                                   if (LocalDate.parse(appointment.getDate()).getYear() == LocalDate.now().getYear())
                                   {
                                       DatabaseHandler.getInstance()
                                                      .getDatabaseReference()
                                                      .child("Users")
                                                      .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                                      .child("NextYear")
                                                      .child(String.valueOf(appointment.getId()))
                                                      .removeValue();

                                       DatabaseHandler.getInstance()
                                                      .getDatabaseReference()
                                                      .child("Users")
                                                      .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                                      .child("Months")
                                                      .child(String.valueOf(LocalDate.parse(appointment.getDate()).getMonthValue() - 1))
                                                      .child("appointments")
                                                      .child(String.valueOf(appointment.getId()))
                                                      .setValue(appointment);
                                   }
                               }
                           }

                           @Override
                           public void onCancelled(@NonNull DatabaseError databaseError)
                           {
                           }
                       });
    }
}
