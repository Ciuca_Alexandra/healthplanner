package com.example.healthplanner.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.example.healthplanner.R;
import com.example.healthplanner.helpers.DatabaseHandler;
import com.example.healthplanner.helpers.MonthsHolder;
import com.example.healthplanner.helpers.UtilsValidators;
import com.example.healthplanner.interfaces.ActivitiesFragmentsCommunication;
import com.example.healthplanner.models.Appointment;
import com.example.healthplanner.models.Month;
import com.google.firebase.auth.FirebaseAuth;

import java.time.LocalDate;

public class AppointmentDetailsFragment extends Fragment
{
    public static final String TAG_APPOINT_DET = "TAG_APPOINT_DET";

    private Month month;
    private Appointment appointment;
    private ActivitiesFragmentsCommunication fragmentsCommunication;
    private EditText edtTitle;
    private EditText edtHour;
    private EditText edtDoctor;
    private EditText edtPhone;
    private EditText edtAddress;

    public static AppointmentDetailsFragment newInstance(Appointment appointment, Month month)
    {
        Bundle args = new Bundle();
        args.putParcelable("appointment", appointment);
        args.putParcelable("month", month);

        AppointmentDetailsFragment fragment = new AppointmentDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_appointment_details, container, false);

        appointment = getArguments().getParcelable("appointment");
        month = getArguments().getParcelable("month");

        edtTitle = view.findViewById(R.id.edt_apt_det);
        edtHour = view.findViewById(R.id.edt_hour_det);
        edtDoctor = view.findViewById(R.id.edt_doctor);
        edtPhone = view.findViewById(R.id.edt_phone);
        edtAddress = view.findViewById(R.id.edt_address);

        edtTitle.setText(appointment.getTitle());
        edtHour.setText(appointment.getTime());
        edtDoctor.setText(appointment.getDoctorName());
        edtPhone.setText(appointment.getPhoneNumber());
        edtAddress.setText(appointment.getAddress());

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        if (context instanceof ActivitiesFragmentsCommunication)
        {
            fragmentsCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.btn_back_apt).setOnClickListener(v ->
        {

            if (LocalDate.parse(appointment.getDate()).getYear() == LocalDate.now().getYear())
            {
                fragmentsCommunication.openApptFragment(month);
            }
            else
            {
                fragmentsCommunication.openNextYearApptFragment();
            }
        });

        view.findViewById(R.id.btn_save_apt).setOnClickListener(v ->
        {
            String title = edtTitle.getText().toString();
            String hour = edtHour.getText().toString();
            String doctor = edtDoctor.getText().toString();
            String phone = edtPhone.getText().toString();
            String address = edtAddress.getText().toString();

            if (TextUtils.isEmpty(title))
            {
                edtTitle.setError("Invalid input");
                return;
            }
            else
            {
                edtTitle.setError(null);
            }

            if (TextUtils.isEmpty(hour))
            {
                edtHour.setError("Invalid input");
                return;
            }
            else if (!UtilsValidators.isValidHour(hour))
            {
                edtHour.setError("Invalid format! (HH:MM)");
                return;
            }
            else
            {
                edtHour.setError(null);
            }

            if (TextUtils.isEmpty(doctor))
            {
                edtDoctor.setText("");
            }
            else
            {
                edtDoctor.setError(null);
            }

            if (TextUtils.isEmpty(phone))
            {
                edtPhone.setText("");
            }
            else if (!UtilsValidators.isValidPhoneNumber(phone))
            {
                edtPhone.setError("Invalid number");
                return;
            }
            else
            {
                edtPhone.setError(null);
            }

            if (TextUtils.isEmpty(address))
            {
                edtAddress.setText("");
            }
            else
            {
                edtAddress.setError(null);
            }

            appointment.setTitle(title);
            appointment.setTime(hour);
            appointment.setDoctorName(doctor);
            appointment.setPhoneNumber(phone);
            appointment.setAddress(address);

            if(month != null)
            {
                DatabaseHandler.getInstance().getDatabaseReference()
                        .child("Users")
                        .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                        .child("Months")
                        .child(String.valueOf(month.getMonthNr() - 1))
                        .child("appointments")
                        .child(String.valueOf(appointment.getId()))
                        .setValue(appointment).addOnCompleteListener(task ->
                {
                    if (task.isSuccessful())
                    {
                        Toast.makeText(getContext(), "Appointment saved!", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(getContext(), "Appointment saving failed!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            else
            {
                DatabaseHandler.getInstance().getDatabaseReference()
                        .child("Users")
                        .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                        .child("NextYear")
                        .child(String.valueOf(appointment.getId()))
                        .setValue(appointment).addOnCompleteListener(task ->
                {
                    if (task.isSuccessful())
                    {
                        Toast.makeText(getContext(), "Appointment saved!", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(getContext(), "Appointment saving failed!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        view.findViewById(R.id.btn_delete).setOnClickListener(v ->
        {
            DatabaseHandler.getInstance().getDatabaseReference()
                           .child("Users")
                           .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                           .child("Months")
                           .child(String.valueOf(month.getMonthNr() - 1))
                           .child("appointments")
                           .child(String.valueOf(appointment.getId()))
                           .removeValue()
                           .addOnCompleteListener(task ->
                           {
                               if (task.isSuccessful())
                               {
                                   Toast.makeText(getContext(), "Appointment deleted!", Toast.LENGTH_SHORT).show();
                                   fragmentsCommunication.openApptFragment(month);
                               }
                               else
                               {
                                   Toast.makeText(getContext(), "Appointment deletion failed!", Toast.LENGTH_SHORT).show();
                               }
                           });
        });
    }
}
