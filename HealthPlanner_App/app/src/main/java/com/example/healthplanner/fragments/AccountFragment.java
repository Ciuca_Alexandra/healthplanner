package com.example.healthplanner.fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.healthplanner.R;
import com.example.healthplanner.helpers.DatabaseHandler;
import com.example.healthplanner.interfaces.ActivitiesFragmentsCommunication;
import com.example.healthplanner.models.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class AccountFragment extends Fragment
{
    public static final String TAG_ACCOUNT = "TAG_ACCOUNT";

    private ActivitiesFragmentsCommunication fragmentsCommunication;
    private EditText edtFirstName;
    private EditText edtLastName;
    private EditText edtDateOfBirth;
    private RadioButton radioButton1;
    private RadioButton radioButton2;

    public static AccountFragment newInstance()
    {
        Bundle args = new Bundle();

        AccountFragment fragment = new AccountFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_account, container, false);

        RadioGroup radiogroup = (RadioGroup) view.findViewById(R.id.acc_radioGroup);
        radiogroup.setOnCheckedChangeListener((group, checkedId) ->
        {
            switch (checkedId)
            {
                case R.id.acc_radioButtonPregnantYes:
                    Toast.makeText(getContext(), "Pregnant checked", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.acc_radioButtonPregnantNo:
                    Toast.makeText(getContext(), "Not pregnant checked", Toast.LENGTH_SHORT).show();
                    break;
            }
        });

        edtFirstName = view.findViewById(R.id.edt_FirstName_acc);
        edtLastName = view.findViewById(R.id.edt_SecondName_acc);
        edtDateOfBirth = view.findViewById(R.id.edt_birthdate_acc);
        radioButton1 = view.findViewById(R.id.acc_radioButtonPregnantYes);
        radioButton2 = view.findViewById(R.id.acc_radioButtonPregnantNo);

        User currentUser = User.getInstance();
        edtFirstName.setText(currentUser.getFirstName());
        edtLastName.setText(currentUser.getSecondName());
        edtDateOfBirth.setText(currentUser.getDateOfBirth());

        if (currentUser.getPregnant())
        {
            radioButton1.setChecked(true);
            radioButton2.setChecked(false);
        }
        else
        {
            radioButton1.setChecked(false);
            radioButton2.setChecked(true);
        }

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        if (context instanceof ActivitiesFragmentsCommunication)
        {
            fragmentsCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.btn_back_acc).setOnClickListener(v -> fragmentsCommunication.openHomeFragment());

        view.findViewById(R.id.btn_save).setOnClickListener(v ->
        {
            edtFirstName = view.findViewById(R.id.edt_FirstName_acc);
            edtLastName = view.findViewById(R.id.edt_SecondName_acc);
            edtDateOfBirth = view.findViewById(R.id.edt_birthdate_acc);
            radioButton1 = view.findViewById(R.id.acc_radioButtonPregnantYes);
            radioButton2 = view.findViewById(R.id.acc_radioButtonPregnantNo);

            String firstName = edtFirstName.getText().toString();
            String lastName = edtLastName.getText().toString();
            String date = edtDateOfBirth.getText().toString();

            if(TextUtils.isEmpty(firstName))
            {
                edtFirstName.setText("");
            }
            else
            {
                edtFirstName.setError(null);
            }

            if(TextUtils.isEmpty(lastName))
            {
                edtLastName.setText("");
            }
            else
            {
                edtLastName.setError(null);
            }

            if(TextUtils.isEmpty(date))
            {
                edtDateOfBirth.setError("Invalid input");
                return;
            }
            else
            {
                edtDateOfBirth.setError(null);
                if(!ProcessDate(edtDateOfBirth))
                {
                    return;
                }
            }

            User.getInstance().setFirstName(edtFirstName.getText().toString());
            User.getInstance().setSecondName(edtLastName.getText().toString());
            User.getInstance().setDateOfBirth(edtDateOfBirth.getText().toString());
            User.getInstance().setPregnant(radioButton1.isChecked());

            DatabaseReference databaseReference = DatabaseHandler.getInstance().getDatabaseReference().child("Users").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
            databaseReference.child("Info").setValue(User.getInstance()).addOnCompleteListener(task ->
            {
                if (task.isSuccessful())
                {
                    Toast.makeText(getContext(), "Profile saved!", Toast.LENGTH_SHORT).show();
                    fragmentsCommunication.openHomeFragment();
                }
                else
                {
                    Toast.makeText(getContext(), "Profile saving failed!", Toast.LENGTH_SHORT).show();
                }
            });
        });
    }

    private boolean ProcessDate(EditText editText)
    {
        try
        {
            LocalDate date = LocalDate.parse(editText.getText().toString());
        }catch (DateTimeParseException e)
        {
            e.printStackTrace();
            editText.setError("Date format invalid! Try yyyy-mm-dd");
            return false;
        }

        return true;
    }
}
