package com.example.healthplanner.interfaces;

import com.example.healthplanner.models.Appointment;
import com.example.healthplanner.models.Month;

public interface OnMonthItemClick
{
    void onClick(int month);
}
