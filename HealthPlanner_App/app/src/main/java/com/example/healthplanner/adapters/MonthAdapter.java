package com.example.healthplanner.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.healthplanner.R;
import com.example.healthplanner.interfaces.OnMonthItemClick;
import com.example.healthplanner.models.Month;

import java.util.ArrayList;

public class MonthAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    ArrayList<Month> months;
    private OnMonthItemClick onMonthItemClick;

    public MonthAdapter(ArrayList<Month> months, OnMonthItemClick onMonthItemClick)
    {
        this.months = months;
        this.onMonthItemClick = onMonthItemClick;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.month_item_cell, parent, false);
        MonthViewHolder monthViewHolder = new MonthViewHolder(view);
        return monthViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if(holder instanceof MonthViewHolder)
        {
            Month month = months.get(position);
            ((MonthViewHolder) holder).bind(month);
        }
    }

    @Override
    public int getItemCount()
    {
        return this.months.size();
    }


    class MonthViewHolder extends RecyclerView.ViewHolder
    {
        private TextView month;
        private View view;

        MonthViewHolder(View view)
        {
            super(view);
            month = view.findViewById(R.id.month_name);
            this.view = view;
        }

        void bind(Month month)
        {
            this.month.setText(month.getName());
            view.setOnClickListener(v ->
            {
                if(onMonthItemClick != null)
                {
                    onMonthItemClick.onClick(month.getMonthNr() - 1);
                }
            });
        }
    }
}
