package com.example.healthplanner.fragments;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.healthplanner.R;
import com.example.healthplanner.adapters.MonthAdapter;
import com.example.healthplanner.helpers.DatabaseHandler;
import com.example.healthplanner.interfaces.ActivitiesFragmentsCommunication;
import com.example.healthplanner.interfaces.OnMonthItemClick;
import com.example.healthplanner.models.Appointment;
import com.example.healthplanner.models.Month;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;

public class HomeFragment extends Fragment
{
    public static final String TAG_HOME = "TAG_HOME";

    private ActivitiesFragmentsCommunication fragmentCommunication;
    private RecyclerView monthRecyclerView;
    private ArrayList<Month> months = new ArrayList<>();

    MonthAdapter monthAdapter = new MonthAdapter(months, new OnMonthItemClick()
    {
        @Override
        public void onClick(int month)
        {
            if (fragmentCommunication != null)
            {
                fragmentCommunication.openApptFragment(months.get(month));
            }
        }
    });

    public static HomeFragment newInstance()
    {
        Bundle args = new Bundle();

        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        monthRecyclerView = view.findViewById(R.id.months_list);
        monthRecyclerView.setHasFixedSize(true);
        monthRecyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        monthRecyclerView.addItemDecoration(new DividerItemDecoration(view.getContext(), DividerItemDecoration.VERTICAL));

        Drawable divider = ContextCompat.getDrawable(view.getContext(), R.drawable.divider);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(view.getContext(), DividerItemDecoration.VERTICAL);
        itemDecoration.setDrawable(divider);

        months.clear();
        DatabaseHandler.getInstance().getDatabaseReference().child("Users").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("Months").addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                months.clear();
                for(DataSnapshot ds : dataSnapshot.getChildren())
                {
                    ArrayList<Appointment> appointments = new ArrayList<>();
                    for(DataSnapshot dsc : ds.child("appointments").getChildren())
                    {
                        Appointment appointment = dsc.getValue(Appointment.class);
                        appointments.add(appointment);
                    }
                    Month month = new Month(ds.child("name").getValue(String.class), ds.child("monthNr").getValue(int.class), appointments);
                    months.add(month);
                }
                monthAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError)
            {
            }
        });

        monthRecyclerView.setAdapter(monthAdapter);

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);

        if (context instanceof ActivitiesFragmentsCommunication)
        {
            fragmentCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        TextView textView = view.findViewById(R.id.txt_year);
        int year = LocalDate.now().getYear();
        textView.setText(String.valueOf(year));
    }
}
