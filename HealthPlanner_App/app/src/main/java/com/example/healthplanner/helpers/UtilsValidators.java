package com.example.healthplanner.helpers;

import android.text.TextUtils;
import android.util.Patterns;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UtilsValidators
{
    public static boolean isValidEmail(String email)
    {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isValidPassword(String password)
    {
        return !TextUtils.isEmpty(password) && password.length() >= 6;
    }

    public static boolean isSamePassword(String firstPassword, String secondPassword)
    {
        return TextUtils.equals(firstPassword, secondPassword);
    }

    public static boolean isValidPhoneNumber(String phone)
    {
        return phone.length()==10 && Patterns.PHONE.matcher(phone).matches();
    }

    public static boolean isValidHour(String hour)
    {
        return Pattern.compile("^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$").matcher(hour).find();
    }
}
