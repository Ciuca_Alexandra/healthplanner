package com.example.healthplanner.interfaces;

import com.example.healthplanner.models.Appointment;
import com.example.healthplanner.models.Month;

public interface ActivitiesFragmentsCommunication
{
    void openWelcomeFragment();
    void openSignUpFragment();
    void openProfileFragment();
    void openLogInFragment();
    void openForgotPasswordFragment();
    void openHomeFragment();
    void openAccountFragment();
    void openRecommTestsFragment();
    void openNextYearApptFragment();
    void openApptFragment(Month month);
    void openAddApptFragment(Month month);
    void openApptDetailsFragment(Appointment appointment, Month month);

    void openMainActivity();
    void openHomeActivity();
}
