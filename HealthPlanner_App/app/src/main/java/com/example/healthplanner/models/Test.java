package com.example.healthplanner.models;

public class Test
{
    private static int lastId = 0;
    private int id;
    private String title;
    private String interval;
    private int minAge;
    private int maxAge;
    private boolean forPregnant;

    public Test()
    {
    }

    public Test(String title, String interval)
    {
        this.id = ++lastId;
        this.title = title;
        this.interval = interval;
        this.minAge = -1;
        this.maxAge = Integer.MAX_VALUE;
        this.forPregnant = false;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getInterval()
    {
        return interval;
    }

    public void setInterval(String interval)
    {
        this.interval = interval;
    }

    public int getMinAge()
    {
        return minAge;
    }

    public void setMinAge(int minAge)
    {
        this.minAge = minAge;
    }

    public int getMaxAge()
    {
        return maxAge;
    }

    public void setMaxAge(int maxAge)
    {
        this.maxAge = maxAge;
    }

    public boolean isForPregnant()
    {
        return forPregnant;
    }

    public void setForPregnant(boolean forPregnant)
    {
        this.forPregnant = forPregnant;
    }
}
