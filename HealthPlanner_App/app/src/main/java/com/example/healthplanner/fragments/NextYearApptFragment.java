package com.example.healthplanner.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.healthplanner.R;
import com.example.healthplanner.adapters.AppointmentAdapter;
import com.example.healthplanner.helpers.DatabaseHandler;
import com.example.healthplanner.interfaces.ActivitiesFragmentsCommunication;
import com.example.healthplanner.interfaces.OnApptItemClick;
import com.example.healthplanner.models.Appointment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;

public class NextYearApptFragment extends Fragment
{
    public static final String TAG_iNCOM_TESTS = "TAG_iNCOM_TESTS";

    private ActivitiesFragmentsCommunication fragmentsCommunication;
    private RecyclerView recyclerView;
    private ArrayList<Appointment> nextAppoints = new ArrayList<>();

    AppointmentAdapter adapter = new AppointmentAdapter(nextAppoints, new OnApptItemClick()
    {
        @Override
        public void onClick(Appointment appointment)
        {
            if (fragmentsCommunication != null)
                fragmentsCommunication.openApptDetailsFragment(appointment, null);
        }
    });

    public static NextYearApptFragment newInstance()
    {
        Bundle args = new Bundle();

        NextYearApptFragment fragment = new NextYearApptFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_nextyear_appt, container, false);

        recyclerView = view.findViewById(R.id.nextYear_appts_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

        DatabaseHandler.getInstance()
                       .getDatabaseReference()
                       .child("Users")
                       .child(FirebaseAuth.getInstance()
                                          .getCurrentUser()
                                          .getUid())
                       .child("NextYear")
                       .addValueEventListener(new ValueEventListener()
                       {
                           @Override
                           public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                           {
                               nextAppoints.clear();
                               for(DataSnapshot ds : dataSnapshot.getChildren())
                               {
                                   Appointment appointment = ds.getValue(Appointment.class);
                                   nextAppoints.add(appointment);
                               }
                               nextAppoints.sort(Comparator.comparingInt(a -> LocalDate.parse(a.getDate()).getDayOfMonth()));
                               adapter.notifyDataSetChanged();
                           }

                           @Override
                           public void onCancelled(@NonNull DatabaseError databaseError)
                           {
                           }
                       });

        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        if (context instanceof ActivitiesFragmentsCommunication)
        {
            fragmentsCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        TextView textView = view.findViewById(R.id.txt_next_appts);
        textView.setText("Next years' appointments: ");

        view.findViewById(R.id.btn_back_incom_tests).setOnClickListener(v -> fragmentsCommunication.openHomeFragment());
    }
}
