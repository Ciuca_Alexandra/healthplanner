package com.example.healthplanner.fragments;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.healthplanner.R;
import com.example.healthplanner.adapters.AppointmentAdapter;
import com.example.healthplanner.helpers.DatabaseHandler;
import com.example.healthplanner.interfaces.ActivitiesFragmentsCommunication;
import com.example.healthplanner.interfaces.OnApptItemClick;
import com.example.healthplanner.models.Appointment;
import com.example.healthplanner.models.Month;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;

public class AppointmentFragment extends Fragment
{
    public static final String TAG_APPOINT = "TAG_APPOINT";

    private ActivitiesFragmentsCommunication fragmentsCommunication;
    private RecyclerView apptRecyclerView;
    private ArrayList<Appointment> appointments = new ArrayList<>();
    private Month month;

    AppointmentAdapter appointmentAdapter = new AppointmentAdapter(appointments, new OnApptItemClick()
    {
        @Override
        public void onClick(Appointment appointment)
        {
            if (fragmentsCommunication != null)
                fragmentsCommunication.openApptDetailsFragment(appointment, month);
        }
    });

    public static AppointmentFragment newInstance(Month month)
    {
        Bundle args = new Bundle();
        args.putParcelable("month", month);

        AppointmentFragment fragment = new AppointmentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_appointmens_list, container, false);

        apptRecyclerView = view.findViewById(R.id.appointment_list);
        apptRecyclerView.setHasFixedSize(true);
        apptRecyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        apptRecyclerView.addItemDecoration(new DividerItemDecoration(view.getContext(), DividerItemDecoration.VERTICAL));

        Drawable divider = ContextCompat.getDrawable(view.getContext(), R.drawable.divider);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(view.getContext(), DividerItemDecoration.VERTICAL);
        itemDecoration.setDrawable(divider);

        month = getArguments().getParcelable("month");

        TextView monthTextView = view.findViewById(R.id.txt_month);
        monthTextView.setText(month.getName());

        DatabaseHandler.getInstance()
                       .getDatabaseReference()
                       .child("Users")
                       .child(FirebaseAuth.getInstance()
                                          .getCurrentUser()
                                          .getUid())
                       .child("Months")
                       .child(String.valueOf(month.getMonthNr() - 1))
                       .child("appointments")
                       .addValueEventListener(new ValueEventListener()
                       {
                           @Override
                           public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                           {
                               appointments.clear();
                               for(DataSnapshot ds : dataSnapshot.getChildren())
                               {
                                   Appointment appointment = ds.getValue(Appointment.class);
                                   appointments.add(appointment);
                               }
                               appointments.sort(Comparator.comparingInt(a -> LocalDate.parse(a.getDate()).getDayOfMonth()));
                               appointmentAdapter.notifyDataSetChanged();
                           }

                           @Override
                           public void onCancelled(@NonNull DatabaseError databaseError)
                           {
                           }
                       });

        apptRecyclerView.setAdapter(appointmentAdapter);

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);

        if (context instanceof ActivitiesFragmentsCommunication)
        {
            fragmentsCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.btn_add_appoint).setOnClickListener(v -> fragmentsCommunication.openAddApptFragment(month));
        view.findViewById(R.id.btn_back_appt_list).setOnClickListener(v -> fragmentsCommunication.openHomeFragment());
    }
}
