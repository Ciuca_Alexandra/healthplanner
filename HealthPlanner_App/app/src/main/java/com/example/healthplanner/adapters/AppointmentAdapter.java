package com.example.healthplanner.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.healthplanner.R;
import com.example.healthplanner.interfaces.OnApptItemClick;
import com.example.healthplanner.models.Appointment;
import com.example.healthplanner.models.Test;

import java.util.ArrayList;

public class AppointmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    ArrayList<Appointment> appointments;
    private OnApptItemClick onApptItemClick;

    public AppointmentAdapter(ArrayList<Appointment> appointments, OnApptItemClick onApptItemClick)
    {
        this.appointments = appointments;
        this.onApptItemClick = onApptItemClick;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.appointment_item_cell, parent, false);
        ApptViewHolder apptViewHolder = new ApptViewHolder(view);
        return apptViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if (holder instanceof ApptViewHolder)
        {
            Appointment appointment = appointments.get(position);
            ((ApptViewHolder) holder).bind(appointment, position);
        }
    }

    @Override
    public int getItemCount()
    {
        return this.appointments.size();
    }


    class ApptViewHolder extends RecyclerView.ViewHolder
    {
        private TextView id;
        private TextView date;
        private TextView title;
        private TextView hour;
        private View view;

        ApptViewHolder(View view)
        {
            super(view);
            id = view.findViewById(R.id.appt_id);
            date = view.findViewById(R.id.appt_date);
            title = view.findViewById(R.id.appointment);
            hour = view.findViewById(R.id.hour);
            this.view = view;
        }

        void bind(Appointment appointment, int index)
        {
            id.setText(String.valueOf(index + 1));
            date.setText(String.valueOf(appointment.getDate()));
            title.setText(appointment.getTitle());
            hour.setText(appointment.getTime());
            view.setOnClickListener(v ->
            {
                if (onApptItemClick != null)
                {
                    onApptItemClick.onClick(appointment);
                }
            });
        }
    }
}
