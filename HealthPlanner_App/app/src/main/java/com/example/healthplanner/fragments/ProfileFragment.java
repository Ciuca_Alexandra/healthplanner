package com.example.healthplanner.fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.healthplanner.R;
import com.example.healthplanner.helpers.DatabaseHandler;
import com.example.healthplanner.helpers.MonthsHolder;
import com.example.healthplanner.interfaces.ActivitiesFragmentsCommunication;
import com.example.healthplanner.models.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class ProfileFragment extends Fragment
{
    public static final String TAG_PROFILE = "TAG_PROFILE";

    private ActivitiesFragmentsCommunication fragmentCommunication;

    public static ProfileFragment newInstance()
    {
        Bundle args = new Bundle();

        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        RadioGroup radiogroup = (RadioGroup) view.findViewById(R.id.radioGroup);
        radiogroup.setOnCheckedChangeListener((group, checkedId) ->
        {
            switch (checkedId)
            {
                case R.id.pregnantRadioButtonYes:
                    Toast.makeText(getContext(), "Pregnant checked", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.pregnantRadioButtonNo:
                    Toast.makeText(getContext(), "Not pregnant checked", Toast.LENGTH_SHORT).show();
                    break;
            }
        });

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);

        if (context instanceof ActivitiesFragmentsCommunication)
        {
            fragmentCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        RadioButton radioBtn3 = view.findViewById(R.id.pregnantRadioButtonYes);
        RadioButton radioBtn4 = view.findViewById(R.id.pregnantRadioButtonNo);

        view.findViewById(R.id.btn_signUp).setOnClickListener(v ->
        {
            EditText dateOfBirthEditText = view.findViewById(R.id.edt_birthdate);
            String date = dateOfBirthEditText.getText().toString();
            if(TextUtils.isEmpty(date))
            {
                dateOfBirthEditText.setError("Invalid input");
                return;
            }
            else
            {
                dateOfBirthEditText.setError(null);
                if(!ProcessDate(dateOfBirthEditText))
                {
                    return;
                }
            }

            if (!validateRadioButtons())
            {
                return;
            }

            User.getInstance().setDateOfBirth(dateOfBirthEditText.getText().toString());

            if (radioBtn3.isChecked())
            {
                User.getInstance().setPregnant(true);
            }
            else if (radioBtn4.isChecked())
            {
                User.getInstance().setPregnant(false);
            }

            DatabaseReference databaseReference = DatabaseHandler.getInstance().getDatabaseReference().child("Users").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
            databaseReference.child("Info").setValue(User.getInstance()).addOnCompleteListener(task ->
            {
                if (task.isSuccessful())
                {
                    databaseReference.child("Months").setValue(MonthsHolder.getInitialMonths()).addOnCompleteListener(task2 ->
                    {
                        if (task2.isSuccessful())
                        {
                            Toast.makeText(getContext(), "User registered!", Toast.LENGTH_SHORT).show();
                            fragmentCommunication.openHomeActivity();
                        }
                        else
                        {
                            Toast.makeText(getContext(), "User registering failed!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                else
                {
                    Toast.makeText(getContext(), "User registering failed!", Toast.LENGTH_SHORT).show();
                }
            });
        });

        view.findViewById(R.id.btn_back_profile).setOnClickListener(v -> fragmentCommunication.openSignUpFragment());
    }

    private boolean validateRadioButtons()
    {
        if (getView() == null)
        {
            return false;
        }

        RadioButton btn = getView().findViewById(R.id.pregnantRadioButtonYes);
        RadioGroup group = getView().findViewById(R.id.radioGroup);

        int nr = group.getCheckedRadioButtonId();

        if (nr <= 0)
        {
            btn.setError("Select an answer.");
            return false;
        }
        return true;
    }

    private boolean ProcessDate(EditText editText)
    {
        try
        {
            LocalDate date = LocalDate.parse(editText.getText().toString());
        }catch (DateTimeParseException e)
        {
            e.printStackTrace();
            editText.setError("Date format invalid! Try yyyy-mm-dd");
            return false;
        }

        return true;
    }
}
